﻿using System.Linq;
using PrimeCounter.Lib.Interfaces;

namespace PrimeCounter.Lib.Checker
{
    public class CheckerCounter : IPrimeCounter
    {
        private readonly IPrimeChecker _checker;

        public CheckerCounter(IPrimeChecker checker)
        {
            _checker = checker;
        }

        public int CountPrimes(int min, int max)
        {
            return Enumerable.Range(min, max - min + 1).Count(_checker.IsPrime);
        }
    }
}
