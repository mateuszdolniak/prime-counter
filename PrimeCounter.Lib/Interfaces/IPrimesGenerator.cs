using System.Collections.Generic;

namespace PrimeCounter.Lib.Interfaces
{
    public interface IPrimesGenerator
    {
        IEnumerable<int> Sieve(int max);
    }
}