﻿using System;
using PrimeCounter.Lib;
using PrimeCounter.Lib.Checker;
using PrimeCounter.Lib.Sieve;

namespace PrimeCounter.App
{
    internal class Program
    {
        private static int GetValue(string name)
        {
            int res;
            do
            {
                Console.Write($"Podaj {name}: ");
            } while (!int.TryParse(Console.ReadLine(), out res));
            return res;
        }

        private static void Main()
        {
            int min = GetValue("min");
            int max = GetValue("max");

            foreach (var prime in PrimeEnumerator.PrimeRange(min, max))
            {
                Console.WriteLine(prime);
            }

            var counter = new SieveCounter(new SieveGenerator());
            //var counter = new CheckerCounter(new PrimeChecker());

            Console.WriteLine($"W przedziale <{min}, {max}> jest {counter.CountPrimes(min, max)} liczb pierwszych");
        }
    }
}
